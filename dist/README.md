# Contents

This folder contains the validated html output files from the autobuild.

* `index.html` contains several elements
    1. header containing number of entries, date of creation and list of links to individual reviewer's html files
    1. compound database inside a singular table, rows ordered alphabetically by the title name
* columns are ordered in following way:
    > title name - automatic from submitted csv's
    > movie/game - automatic from submitted csv's
* ^ those two are considered a key
    > genre - top three most used descriptions of genre by reviewers separated by `/` in order of frequency
    > imdb search query link - automatic based on title
    > metacritic search query link - automatic based on title
    > rottentomatoes search query link - automatic based on title
* sequence of columns for each reviewer, order of columns by default alphabetical
    > reviewer's link (from csv's)
    > reviewer's scoring (from csv's)

* `{reviewername}.html` - individual reviewer's table
    > rows ordered alphabetically
    > columns same as in the main `index.html` but only this reviewer's data is shown
