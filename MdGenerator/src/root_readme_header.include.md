# Alternative MD

## What

* This is a semi-automatically generated table of movie and game video reviews from select reviewers on youtube.
* The list is validated to a certain date, see below.
* If you wish to use this data, feel free to do so, provided you follow the licence included.
* If you are a video reviewer and wish to be included in this list, see the `CONTRIBUTING.md` file for instructions.

## Why

* First motivation is to have a personal look up table for inspiration what to watch or after watching something, compare my own impressions with those of other people, who decide to share those impressions with the public on YouTube and over time become reviewers
* This isn't a tool for promoting one reviewer over another, and multiple precautions are taken to insure this.
* My wish is that this public repository of publicly available data will be eventually maintained by mutliple people to insure maximum possible level of up-to-date-ness and accuracy
* The entry threshold of 100 valid video reviews is an estimate of what could be considered someone dedicated to making those reviews consistently. This is just an estimate that could be adjusted in the future.
