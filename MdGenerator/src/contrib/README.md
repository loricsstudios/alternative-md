# Contents

* This folder contains the CSV files submitted by the reviewers.
* Reviewers can use the YoutubeParser subproject to generate drafts of those CSV's to simplify their maintenance work.
* Manual work f.e. involves adding data for the rating data column (4th)
* Optionally add genere into 5th
* Detailed use description of the YoutubeParser subproject is in that folder's README.md
* Currently contained csv's are samples created manually from the list of videos of a couple of reviewers.